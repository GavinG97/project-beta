function CustomerList(props) {
    return (
      <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Customer List</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Phone Number</th>
              <th>Address</th>
            </tr>
          </thead>
          <tbody>
            {props.customers.map(customer => {
            return (
              <tr>
                <td>{ customer.first_name }</td>
                <td>{ customer.last_name }</td>
                <td>{ customer.phone_number }</td>
                <td>{ customer.address }</td>
              </tr>
            );
          })}
          </tbody>
        </table>
        </p>
      </div>
    </div>
      );
}
export default CustomerList;
