import React from "react"
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ServiceList from "./ServiceList";
import CustomerList from './CustomerList';
import SalesList from './SalesList'
import AutosList from './AutosList'
import ManufacturersList from './ManufacturersList';
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import ServiceForm from "./ServiceForm";
import ServiceHistory from "./ServiceHistory";
import SalespeopleList from './SalespeopleList';
import ModelList from './ModelList';
import CustomerForm from './CustomerForm';
import SalespersonForm from './SalespersonForm';
import ManufacturerForm from './ManufacturerForm';
import ModelForm from './ModelForm';
import SalespersonHistory from './SalespersonHistory';
import SalesForm from './SalesForm';
import AutomobileForm from './AutomobileForm';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}/>
          <Route path="customers">
            <Route index element={<CustomerList customers={props.customers} />}/>
            <Route path="new" element={<CustomerForm />}/>
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList salesperson={props.salesperson} />}/>
            <Route path="new" element={<SalespersonForm />}/>
          </Route>
          <Route path="sales">
            <Route index element={<SalesList sales={props.sales} />}/>
            <Route path="history" element={<SalespersonHistory />}/>
            <Route path="new" element={<SalesForm /> }/>
          </Route>
          <Route path="automobiles">
            <Route index element={<AutosList autos={props.autos} />}/>
            <Route path="new" element={<AutomobileForm /> }/>
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturersList manufacturers={props.manufacturers} />}/>
            <Route path="new" element={<ManufacturerForm />}/>
          </Route>
          <Route path="/appointments">
            <Route index element={<ServiceList appointments={props.appointments} />} />
            <Route path="new" element={<ServiceForm />} />
            <Route path="history" element={<ServiceHistory appointments={props.appointments} />} />
          </Route>
          <Route path="/technicians">
            <Route index element={<TechnicianList technician={props.technician} />} />
            <Route path="new" element={<TechnicianForm />} />
          </Route>
          <Route path="/models">
          <Route index element={<ModelList models={props.models} />} />
          <Route path="new" element={<ModelForm/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
