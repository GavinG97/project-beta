function SalespeopleList(props) {
    return (
        <div className="px-4 py-5 my-5 text-center">
          <h1 className="display-5 fw-bold">Salespeople List</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
            <table className="table table-striped">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Employee ID</th>
              </tr>
            </thead>
            <tbody>
              {props.salesperson.map(salesperson => {
              return (
                <tr>
                  <td>{ salesperson.first_name }</td>
                  <td>{ salesperson.last_name }</td>
                  <td>{ salesperson.employee_id }</td>
                </tr>
              );
            })}
            </tbody>
          </table>
            </p>
          </div>
        </div>
      );
}
export default SalespeopleList;
