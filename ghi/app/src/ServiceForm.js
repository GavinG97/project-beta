import React from 'react';

class ServiceForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      date_time: '',
      reason: '',
      vin: '',
      technician: '',
      status: '',
      customer: '',
      automobiles: [],
      technicians: [],
    };
    this.handleChangeDate_time= this.handleChangeDate_time.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
    this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
    this.handleChangeStatus = this.handleChangeStatus.bind(this);
    this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const response1 = await fetch(automobileUrl);
    if (response1.ok) {
      const data = await response1.json();
      this.setState({ automobiles: data.automobiles });
    }
    const response2 = await fetch(technicianUrl);
    if (response2.ok) {
      const data = await response2.json();
      console.log(data.technician)
      this.setState({ technicians: data.technician });
    }
  }

  handleChangeDate_time(event) {
    const value = event.target.value;
    this.setState({ date_time: value })
  }

  handleChangeReason(event) {
    const value = event.target.value;
    this.setState({ reason: value })
  }

  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({ vin: value })
  }

  handleChangeTechnician(event) {
    const value = event.target.value;
    this.setState({ technician: value })
  }

  handleChangeStatus(event) {
    const value = event.target.value;
    this.setState({ status: value })
  }
  handleChangeCustomer(event) {
    const value = event.target.value;
    this.setState({ customer: value })
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.technicians;

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);

      this.setState({
        date_time: '',
        reason: '',
        vin: '',
        technician: '',
        status: '',
        customer: '',
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Service</h1>
            <form onSubmit={this.handleSubmit} id="create-appointments-form">
            <div className="form-floating mb-3">
                <input value={this.state.date_time} onChange={this.handleChangeDate_time} placeholder="Date Time" required type="datetime-local" id="date_time" className="form-control" />
                <label htmlFor="date_time">Date Time</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.reason} onChange={this.handleChangeReason} placeholder="Reason" required type="text" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.vin} onChange={this.handleChangeVin} placeholder="Vin" required type="text" id="vin" className="form-control" />
                <label htmlFor="vin">Vin</label>
            </div>
            <div className="form-floating mb-3">
                <input value={this.state.customer} onChange={this.handleChangeCustomer} placeholder="Customer" required type="text" id="customer" className="form-control" />
                <label htmlFor="vin">Customer</label>
            </div>
            <div className="form-floating mb-3">
                <label htmlFor="technician"></label>
                <select value={this.state.technician} onChange={this.handleChangeTechnician} placeholder="Technician" id="technician" className="form-control" >
                    <option value="">Choose Your Technician</option>
                    {this.state.technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>{technician.first_name}</option>
                    )
                    })}
                </select>
            </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceForm;
