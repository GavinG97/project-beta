function SalesList(props) {
    return (
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Sales List</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Salesperson Employee ID</th>
              <th>Salesperson Name</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {props.sales.map(sales => {
            return (
              <tr>
                <td>{ sales.salesperson.employee_id }</td>
                <td>{ sales.salesperson.first_name } { sales.salesperson.last_name }</td>
                <td>{ sales.customer.first_name } { sales.customer.last_name }</td>
                <td>{ sales.automobile.vin }</td>
                <td>{ sales.price }</td>
              </tr>
            );
          })}
          </tbody>
        </table>
          </p>
        </div>
      </div>
    );
  }

  export default SalesList;
